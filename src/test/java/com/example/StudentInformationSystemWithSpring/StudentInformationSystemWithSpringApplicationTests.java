package com.example.StudentInformationSystemWithSpring;

import com.example.StudentInformationSystemWithSpring.controller.services.SectionService;
import com.example.StudentInformationSystemWithSpring.model.Sections;
import jdk.jfr.Name;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class StudentInformationSystemWithSpringApplicationTests {
    @Autowired
    SectionService sectionService;

    @DisplayName("CheckSection")
	@Test
	void contextLoads() {
        System.out.println(sectionService.getAll().size());
        Assertions.assertEquals(6, sectionService.getAll().size());
        if(sectionService.getAll().size() != 6){
            sectionService.addSection(new Sections("Saint Francis"));
            sectionService.addSection(new Sections("Saint Paul"));
            sectionService.addSection(new Sections("Saint Therese"));
            sectionService.addSection(new Sections("Saint John"));
            sectionService.addSection(new Sections("Saint Peter"));
            sectionService.addSection(new Sections("Saint Luke"));
        }
	}

}
