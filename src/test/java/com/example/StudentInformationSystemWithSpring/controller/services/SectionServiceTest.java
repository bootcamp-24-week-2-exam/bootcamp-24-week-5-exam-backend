package com.example.StudentInformationSystemWithSpring.controller.services;

import com.example.StudentInformationSystemWithSpring.controller.SectionController;
import com.example.StudentInformationSystemWithSpring.model.Sections;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(controllers = SectionController.class)
@ExtendWith(SpringExtension.class)
class SectionServiceTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    SectionService sectionService;

    @Autowired
    private ObjectMapper objectMapper;

    @Test
    void getAll() throws Exception{
//        mockMvc.perform(MockMvcRequestBuilders.get("/sections")).andExpect(status().is2xxSuccessful());

        // given - precondition or setup
        List<Sections> listOfSections = new ArrayList<>();
        listOfSections.add(Sections.builder().id(1).section("Saint Francis").build());
        listOfSections.add(Sections.builder().id(2).section("Saint Paul").build());
        given(sectionService.getAll()).willReturn(listOfSections);

        // when -  action or the behaviour that we are going test
        ResultActions response = mockMvc.perform(MockMvcRequestBuilders.get("/sections")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(listOfSections)));

        // then - verify the output
        response.andExpect(status().isOk())
                .andDo(print())
                .andExpect(MockMvcResultMatchers.jsonPath("$.size()",
                        CoreMatchers.is(listOfSections.size())));
    }
}