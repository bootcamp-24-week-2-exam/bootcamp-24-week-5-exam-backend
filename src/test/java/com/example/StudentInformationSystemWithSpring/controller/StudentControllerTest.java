package com.example.StudentInformationSystemWithSpring.controller;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;


public class StudentControllerTest {

    private URL urlFull;
    private String url = "localhost:8080";

    // test for home page response
    @Test
    public void homeTest() {
        try {
            urlFull = new URL(url);
            HttpURLConnection httpURLConnection = (HttpURLConnection) urlFull.openConnection();
            Assertions.assertEquals(200, httpURLConnection);
        } catch (IOException e) {
            Assertions.assertFalse(false);
        }
    }

    // test for add student response
    @Test
    public void addStudentTest()   {
        url = url + "/add";
        try {
            urlFull = new URL(url);
            HttpURLConnection httpURLConnection = (HttpURLConnection) urlFull.openConnection();
            Assertions.assertEquals(200, httpURLConnection);
        } catch (IOException e) {
            Assertions.assertFalse(false);
        }
    }

    // test for edit student response
    @Test
    public void editStudent()   {
        // mock id.
        int id = 1;
        url = url + "/edit/" + id;
        try {
            urlFull = new URL(url);
            HttpURLConnection httpURLConnection = (HttpURLConnection) urlFull.openConnection();
            Assertions.assertEquals(200, httpURLConnection);
        } catch (IOException e) {
            Assertions.assertFalse(false);
        }
    }

    // test for delete student response
    @Test
    public void deleteStudent()   {
        // mock id.
        int id = 2;
        url = url + "/delete/" + id;
        try {
            urlFull = new URL(url);
            HttpURLConnection httpURLConnection = (HttpURLConnection) urlFull.openConnection();
            Assertions.assertEquals(200, httpURLConnection);
        } catch (IOException e) {
            Assertions.assertFalse(false);
        }
    }

    // test for search student response
    @Test
    public void search() {
        url = url + "/search/sort";
        try {
            urlFull = new URL(url);
            HttpURLConnection httpURLConnection = (HttpURLConnection) urlFull.openConnection();
            Assertions.assertEquals(200, httpURLConnection);
        } catch (IOException e) {
            Assertions.assertFalse(false);
        }
    }

    // test for search by year level response
    @Test
    public void searchByYrLvl()   {
        url = url + "/search/byYrLvl/";
        try {
            urlFull = new URL(url);
            HttpURLConnection httpURLConnection = (HttpURLConnection) urlFull.openConnection();
            Assertions.assertEquals(200, httpURLConnection);
        } catch (IOException e) {
            Assertions.assertFalse(false);
        }
    }


    // test for search by section response
    @Test
    public void searchBySec()   {
        url = url + "/search/bySec";
        try {
            urlFull = new URL(url);
            HttpURLConnection httpURLConnection = (HttpURLConnection) urlFull.openConnection();
            Assertions.assertEquals(200, httpURLConnection);
        } catch (IOException e) {
            Assertions.assertFalse(false);
        }
    }

    // test for search by city response
    @Test
    public void searchByCity()   {
        url = url + "/search/byCity";
        try {
            urlFull = new URL(url);
            HttpURLConnection httpURLConnection = (HttpURLConnection) urlFull.openConnection();
            Assertions.assertEquals(200, httpURLConnection);
        } catch (IOException e) {
            Assertions.assertFalse(false);
        }
    }

    // test for search by ZIP response
    @Test
    public void searchByZip()   {
        url = url + "/search/byZip";
        try {
            urlFull = new URL(url);
            HttpURLConnection httpURLConnection = (HttpURLConnection) urlFull.openConnection();
            Assertions.assertEquals(200, httpURLConnection);
        } catch (IOException e) {
            Assertions.assertFalse(false);
        }
    }

    // test for search by ageresponse
    @Test
    public void searchByAge()   {
        url = url + "/search/byAge";
        try {
            urlFull = new URL(url);
            HttpURLConnection httpURLConnection = (HttpURLConnection) urlFull.openConnection();
            Assertions.assertEquals(200, httpURLConnection);
        } catch (IOException e) {
            Assertions.assertFalse(false);
        }
    }
}
