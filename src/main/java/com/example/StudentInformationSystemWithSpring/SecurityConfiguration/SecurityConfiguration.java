package com.example.StudentInformationSystemWithSpring.SecurityConfiguration;


import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@EnableWebSecurity
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {

        // bcrypt encoder for encryption.
        BCryptPasswordEncoder bCryptPasswordEncoder = (BCryptPasswordEncoder) passwordEncoder();

        // user list.
        auth.inMemoryAuthentication()
                .withUser("root")
                .password(bCryptPasswordEncoder.encode("root"))
                .authorities("ROLE_ADMIN");
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.cors().and().csrf().disable()
                .authorizeRequests()
                .antMatchers(HttpMethod.POST, "/add").hasAuthority("ROLE_ADMIN")
                .antMatchers(HttpMethod.PUT, "/edit").hasAuthority("ROLE_ADMIN")
                .antMatchers(HttpMethod.DELETE, "/delete").hasAuthority("ROLE_ADMIN")
                .antMatchers(HttpMethod.GET, "/search/sort").hasAuthority("ROLE_ADMIN")
                .antMatchers(HttpMethod.GET, "/search").hasAuthority("ROLE_ADMIN")
                .antMatchers(HttpMethod.GET, "/").permitAll()
                .and().httpBasic()
                .and().formLogin();
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }
}
