package com.example.StudentInformationSystemWithSpring.controller.services;

import com.example.StudentInformationSystemWithSpring.model.Student;
import com.example.StudentInformationSystemWithSpring.repository.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class StudentSearchService implements StudentSearchServiceInterface{

    @Autowired
    StudentRepository studentRepository;

    public List<Student> getStudSortByNameYrLvlSec(){
        return studentRepository.findByOrderByLastnameAscSectionIdAscYrlvlAsc();
    }

    public List<Student> getStudPerYrLvl(int ylvl) {
        return studentRepository.findByYrlvlOrderByLastnameAsc(ylvl);
    }
//
//    public List<Student> getStudPerSec(String section) {
//        return null;
//    }

    public List<Student> getStudPerSec(int sectionId) {
        return studentRepository.findBysectionIdOrderByLastnameAsc(sectionId);
    }

    public List<Student> getStudPerCity(String city) {
        return studentRepository.findByCityOrderByLastnameAsc(city);
    }

    public List<Student> getStudPerZip(int zip) {
        return studentRepository.findByZipOrderByLastnameAsc(zip);
    }

    public List<Student> getStudPerAgeBracket(int min, int max) {
        return studentRepository.findByAgeBetweenOrderByLastnameAsc(min, max);
    }

    public List<Student> getAll(){
        return studentRepository.findAll();
    }

    public List<Student> getByLastnameFirstnameMiddlenameId(String Lastname, String Firstname, String Middlename, int Id){
        return studentRepository.findByLastnameLikeOrFirstnameLikeOrMiddlenameLikeOrId(Lastname, Firstname, Middlename, Id);
    }

    public List<Student> getStudents(){
        return studentRepository.findStudents();
    }

}
