package com.example.StudentInformationSystemWithSpring.controller.services;


import com.example.StudentInformationSystemWithSpring.model.Student;
import com.example.StudentInformationSystemWithSpring.repository.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestParam;

@Service
public class StudentService {

    @Autowired
    StudentRepository studentRepository;


    //Adding student.
    public void AddStudent(Student student) {
        studentRepository.save(student);
    }

    public void EditStudent(Student student, Integer id) {
        student.setId(id);
        studentRepository.save(student);
    }

    public void DeleteStudent(Integer id) {
        studentRepository.deleteById(id);
    }
}
