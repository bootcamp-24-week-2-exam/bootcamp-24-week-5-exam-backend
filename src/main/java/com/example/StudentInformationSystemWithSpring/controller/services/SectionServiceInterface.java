package com.example.StudentInformationSystemWithSpring.controller.services;

import com.example.StudentInformationSystemWithSpring.model.Sections;

import java.util.List;

public interface SectionServiceInterface {
    List<Sections> getAll();
}
