package com.example.StudentInformationSystemWithSpring.controller;

import com.example.StudentInformationSystemWithSpring.controller.services.SectionService;
import com.example.StudentInformationSystemWithSpring.model.Sections;
import com.example.StudentInformationSystemWithSpring.model.Student;
import netscape.javascript.JSObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <H1>Section Information Controller</H1>
 * <p>This class use to get the Section Information</p>
 * <p>This is for Mapping to GetMapping</p>
 *
 * @author samuel.guban
 * @since 2022-08-22
 */
@RestController
@RequestMapping("/")
@CrossOrigin("http://localhost:3000")
public class SectionController {

    @Autowired
    SectionService sectionService;

    /**
     * This method use to get the section list
     * @return Section list
     */
    @GetMapping("/sections")
    public List<Sections> searchByAll(){
        return sectionService.getAll();
    }
}
