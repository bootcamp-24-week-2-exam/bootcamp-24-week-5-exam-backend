package com.example.StudentInformationSystemWithSpring.repository;

import com.example.StudentInformationSystemWithSpring.model.Student;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface StudentRepository extends JpaRepository<Student, Integer> {
    List<Student> findByOrderByLastnameAscSectionIdAscYrlvlAsc();
    List<Student> findByYrlvlOrderByLastnameAsc(int yrlvl);
    List<Student> findBysectionIdOrderByLastnameAsc(int sectionId);
    List<Student> findByCityOrderByLastnameAsc(String city);
    List<Student> findByZipOrderByLastnameAsc(int zip);
    List<Student> findByAgeBetweenOrderByLastnameAsc(int min, int max);
    List<Student> findAll();
    List<Student> findByLastnameLikeOrFirstnameLikeOrMiddlenameLikeOrId(String Lastname, String Firstname, String Middlename, int Id);

    String FIND_STUDENTS = "SELECT st.stud_id, st.stud_firstname, st.stud_lastname, st.stud_middlename, st.stud_address, st.stud_age, st.stud_zip, st.stud_city, st.stud_regionProvince, st.stud_phoneNo, st.stud_mobileNo, st.stud_email, st.stud_yrlvl, st.stud_sectionId, sec.sec_section FROM tbl_students AS st LEFT JOIN tbl_sections AS sec ON st.stud_sectionId = sec.sec_id order by st.stud_id";

    @Query(value = FIND_STUDENTS, nativeQuery = true)
    List<Student> findStudents();
}
