package com.example.StudentInformationSystemWithSpring.repository;

import com.example.StudentInformationSystemWithSpring.model.Sections;
import com.example.StudentInformationSystemWithSpring.model.Student;
import netscape.javascript.JSObject;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface SectionRepository extends JpaRepository<Sections, Long> {
    List<Sections> findAll();

    public static final String FIND_PROJECTS = "SELECT sec_id, sec_section FROM tbl_sections";

    @Query(value = FIND_PROJECTS, nativeQuery = true)
    public List<Object> findProjects();
}
