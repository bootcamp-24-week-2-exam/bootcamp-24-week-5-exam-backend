package com.example.StudentInformationSystemWithSpring;

import com.example.StudentInformationSystemWithSpring.controller.services.SectionService;
import com.example.StudentInformationSystemWithSpring.model.Sections;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author samuel.guban
 * @author paul.samson
 * @author lawrence.chan
 * @since 2022-08-22
 */
@SpringBootApplication
public class StudentInformationSystemWithSpringApplication implements CommandLineRunner {
	@Autowired
	SectionService sectionService;

	/**
	 * @param args the command line arguments - unused
	 */
	public static void main(String[] args) {
		SpringApplication.run(StudentInformationSystemWithSpringApplication.class, args);

	}

	/**
	 * To check if Section have a data
	 * if no data, add the 6 sections
	 * @param args incoming main method arguments
	 * @throws Exception
	 */
	@Override
	public void run(String... args) throws Exception {
		System.out.println(sectionService.getAll().size());
		if(sectionService.getAll().size() != 6){
			sectionService.addSection(new Sections("Saint Francis"));
			sectionService.addSection(new Sections("Saint Paul"));
			sectionService.addSection(new Sections("Saint Therese"));
			sectionService.addSection(new Sections("Saint John"));
			sectionService.addSection(new Sections("Saint Peter"));
			sectionService.addSection(new Sections("Saint Luke"));
		}
	}
}
