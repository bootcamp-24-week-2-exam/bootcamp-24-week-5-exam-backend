package com.example.StudentInformationSystemWithSpring.model;


import lombok.Builder;
import lombok.Data;
import lombok.Generated;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 *  <H1>Section Information</H1>
 * <p>This class use to set ang get the Section Information</p>
 *
 * @author samuel.guban
 * @author lawrence.chan
 * @since 2022-08-22
 */
@Entity(name = "tbl_sections")
@Data
@Builder
public class Sections {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "sec_id")
    private Integer id;

    @Column (name = "sec_section")
    private String section;

//    @OneToMany(mappedBy = "sections", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
//    private List<Student> studentList = new ArrayList<>();

    /**
     * This is constructor - unused
     */
    public Sections() {
    }

    /**
     * Construct the Section information
     * @param id
     * @param section
     */
    public Sections(Integer id, String section) {
        this.id = id;
        this.section = section;
    }

    public Sections(String section) {
        this.section = section;
    }

    /**
     * To get section id
     * @return
     */
    public Integer getId() {
        return id;
    }

    /**
     * To set section id
     * @param id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * To get section
     * @return section
     */
    public String getSection() {
        return section;
    }

    /**
     * To set ssection
     * @param section
     */
    public void setSection(String section) {
        this.section = section;
    }

//    public List<Student> getStudentList() {
//        return studentList;
//    }
//
//    public void setStudentList(List<Student> studentList) {
//        this.studentList = studentList;
//    }


}
